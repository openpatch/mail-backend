import os
import json
import pika
import requests
import logging
from retry import retry
import sentry_sdk

logging.basicConfig(level=logging.INFO)

_DELIVERY_MODE_PERSISTENT = 2

sentry_dsn = os.getenv("SENTRY_DSN", None)

if sentry_dsn:
    sentry_sdk.init(dsn=sentry_dsn, environment=os.getenv("OPENPATCH_MODE"))


class Worker:
    def __init__(self, domain, mailgun_api_key, rabbitmq_host, retry_delay_ms):
        self.domain = domain
        self.mailgun_api_key = mailgun_api_key
        if not self.mailgun_api_key:
            logging.error("No MAILGUN_API_KEY")
        self.rabbitmq_host = rabbitmq_host
        self.retry_delay_ms = retry_delay_ms

    @retry(pika.exceptions.AMQPConnectionError, delay=5, jitter=(1, 3))
    def consume(self):
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=RABBITMQ_HOST)
        )
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue="mail_queue", durable=True)

        self.channel.exchange_declare(
            exchange="mail_exchange", exchange_type="direct", durable=True
        )
        self.channel.queue_bind(exchange="mail_exchange", queue="mail_queue")

        self.retry_channel = self.connection.channel()
        self.retry_channel.queue_declare(
            queue="retry_mail_queue",
            durable=True,
            arguments={
                "x-message-ttl": self.retry_delay_ms,
                "x-dead-letter-exchange": "amq.direct",
                "x-dead-letter-routing-key": "mail_queue",
            },
        )

        self.channel.basic_consume("mail_queue", self.process_mail_queue)
        try:
            self.channel.start_consuming()
            logging.info("Connected to Rabbitmq {}", self.rabbitmq_host)
        except pika.exceptions.ConnectionClosedByBroker:
            logging.warning("Connect to Rabbitmq {} closed", self.rabbitmq_host)

    def process_mail_queue(self, ch, method, properties, body):
        try:
            mail = json.loads(body)
        except json.decoder.JSONDecodeError:
            logging.error("JSON is not correctly formatted. Not sending mail")
            return
        to = mail.get("to")
        subject = mail.get("subject")
        text = mail.get("text")
        html = mail.get("html")
        data = {
            "from": "OpenPatch <noreply@{}>".format(self.domain),
            "to": [to],
            "subject": subject,
            "text": text,
            "html": html,
        }
        logging.info("Sending mail: {}".format(json.dumps(data)))
        if self.mailgun_api_key != "MOCK":
            api = "https://api.eu.mailgun.net/v3/{}/messages".format(self.domain)

            if not (to and subject and (text or html)):
                logging.warning("Missing information. Not sending mail")
                return

            res = requests.post(api, auth=("api", self.mailgun_api_key), data=data)
            ch.basic_ack(delivery_tag=method.delivery_tag)
            if res.status_code != 200 and res.status_code != 404:
                logging.warning(
                    "Error sending to {}. {} {}. Retrying...".format(
                        to, res.status_code, res.reason
                    )
                )

                self.retry_channel.basic_publish(
                    exchange="mail_exchange",
                    routing_key="retry_mail_queue",
                    body=body,
                    properties=pika.BasicProperties(
                        delivery_mode=_DELIVERY_MODE_PERSISTENT
                    ),
                )


if __name__ == "__main__":
    # Configuration
    DOMAIN = os.getenv("DOMAIN", "mail.openpatch.app")
    MAILGUN_API_KEY = os.getenv("MAILGUN_API_KEY")
    RABBITMQ_HOST = os.getenv("RABBITMQ_HOST")
    RETRY_DELAY_MS = os.getenv("RETRY_DELAY_MS", 30000)
    worker = Worker(DOMAIN, MAILGUN_API_KEY, RABBITMQ_HOST, RETRY_DELAY_MS)
    worker.consume()
