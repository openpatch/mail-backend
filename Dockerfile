FROM python:3.8

COPY . /var/www/app
WORKDIR /var/www/app
RUN pip install -r requirements.txt

ENV OPENPATCH_MODE development

CMD ["python", "worker.py"]
