# Enviroment Variables

* DOMAIN default to mail.openpatch.app
* MAILGUN_API_KEY the mailgun api key, if equals MOCK the email will
  be printed and not sent to mailgun
* RABBITMQ_HOST the url of the rabbitMQ host

# Usage

This services connects to a rabbit mq broker and will consume messages, which
are send to the "mail_queue" via "mail_exchange". These messages need to have a json body of this
structure:

```
{
  "to": "email@address.eu",
  "subject": "Subject",
  "text": "Mail content in plain text",
  "html": "Mail content in html"
}
```

to and subject are required. You can choose to send text or html or both, but
one needs to be sent.
